package ibs_lab1

class DepositType {

    static constraints = {
    }

    String alias
    String description
}
