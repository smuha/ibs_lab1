package ibs_lab1

class Counterparty {

    static constraints = {
    }

    String userName
    String userSurName
    String userMiddleName
    String userAddress
    String workPlace
    Date dateCreated
    Date lastUpdated
    CounterpartyType counterpartyType
}
