package ibs_lab1

class DepositInterestRateType {

    static constraints = {
    }

    String alias
    String description
}
