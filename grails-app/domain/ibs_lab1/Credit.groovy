package ibs_lab1

class Credit {

    static constraints = {
    }

    Counterparty counterparty
    int loanAmount
    int numberOfMonth
    float interestRate
    Date dateCreated
    Date lastUpdated
    CreditProgram creditProgram
    float finalResult
    float payAmount
}
