package ibs_lab1

class Deposit {

    static constraints = {
    }

    Counterparty counterparty
    DepositType depositType
    DepositInterestRateType interestRateType
    float depositAmount
    float interestRate
    int frequency   //цикличность начисления процентов (month)
    int depositTerm //(month)
    float result
}