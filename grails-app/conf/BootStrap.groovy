import ibs_lab1.*

class BootStrap {

    def init = { servletContext ->
        generateCounterpartyTypes()
        generateCounterparties()
        generateCreditPrograms()
        generateCredits()
        generateDepositTypes()
        generateDepositInterestRateTypes()
        generateDeposits()
    }

    private generateDeposits() {
        10.times {
            new Deposit(
                    counterparty: Counterparty.executeQuery('from Counterparty order by rand()', [max: 1]),
                    depositType: DepositType.executeQuery('from DepositType order by rand()', [max: 1]),
                    interestRateType: DepositInterestRateType.executeQuery('from DepositInterestRateType order by rand()', [max: 1]),
                    depositAmount: getRandomNumber(100000),
                    interestRate: getRandomNumber(30),
                    frequency: getRandomNumber(12),
                    depositTerm: getRandomNumber(36)
            ).save()
        }
    }

    private void generateDepositInterestRateTypes() {
        new DepositInterestRateType(
                alias: "Interest rate #1",
                description: "З капіталізацією відсотків"
        ).save()
        new DepositInterestRateType(
                alias: "Interest rate #2",
                description: "Без капіталізації відсотків"
        ).save()
    }

    private void generateDepositTypes() {
        new DepositType(
                alias: "Deposit #1",
                description: "This is the first deposit type"
        ).save()
        new DepositType(
                alias: "Deposit #2",
                description: "This is the second deposit type"
        ).save()
        new DepositType(
                alias: "Deposit #3",
                description: "This is the third deposit type"
        ).save()
    }

    private void generateCredits() {
        10.times {
            new Credit(
                    counterparty: Counterparty.executeQuery('from Counterparty order by rand()', [max: 1]),
                    creditProgram: CreditProgram.executeQuery('from CreditProgram order by rand()', [max: 1]),
                    loanAmount: getRandomNumber(20000),
                    numberOfMonth: getRandomNumber(24),
                    interestRate: getRandomNumber(30),
                    payAmount: 500
            ).save()
        }
    }

    private void generateCreditPrograms() {
        new CreditProgram(
                programAlias: "Program #1",
                programDescription: "Середньомісячний внесок по кредиту = Сума кредиту / строк (місяців)"
        ).save()
        new CreditProgram(
                programAlias: "Program #2",
                programDescription: "Відсоток до сплати (процентна ставка / 12) * залишок боргу по кредиту (для першого методу)"
        ).save()
    }

    private void generateCounterpartyTypes() {
        if (!CounterpartyType.findByTypeName("Фізична особа")) {
            new CounterpartyType(typeName: "Фізична особа").save(flush: true, failOnError: true)
        }
        if (!CounterpartyType.findByTypeName("Юридична особа")) {
            new CounterpartyType(typeName: "Юридична особа").save(flush: true, failOnError: true)
        }
    }

    private void generateCounterparties() {
        new Counterparty(
                counterpartyType: CounterpartyType.findByTypeName("Фізична особа"),
                userName: "Вячеслав",
                userSurName: "Муха",
                userMiddleName: "Олександрович",
                userAddress: "м. Черкаси, вул. Калініна 143/4",
                workPlace: "Fireblink"
        ).save(flush: true, failOnError: true)
        new Counterparty(
                counterpartyType: CounterpartyType.findByTypeName("Фізична особа"),
                userName: "Олег",
                userSurName: "Поліщук",
                userMiddleName: "Батькович",
                userAddress: "м. Черкаси, вул. Леніна 12, кв 1",
                workPlace: "Fireblink"
        ).save(flush: true, failOnError: true)
        new Counterparty(
                counterpartyType: CounterpartyType.findByTypeName("Фізична особа"),
                userName: "Артем",
                userSurName: "Мигвич",
                userMiddleName: "Батькович",
                userAddress: "м. Черкаси, вул. Жовтнева 162, кв 3",
                workPlace: "FireBlink"
        ).save(flush: true, failOnError: true)
    }

    int getRandomNumber(int max) {
        return Math.abs(new Random().nextInt() % max + 1)
    }

    def destroy = {
    }
}
