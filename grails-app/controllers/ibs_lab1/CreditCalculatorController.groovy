package ibs_lab1

class CreditCalculatorController {

    def index() {
        List<Credit> credits = Credit.findAll()
        credits.each {
            def averageAmount = it.loanAmount / it.numberOfMonth
            if (it.creditProgram.id == 1) {
                it.finalResult = averageAmount
            } else {
                it.finalResult = (it.interestRate / 12) *  averageAmount
            }
        }
        respond credits
    }

    // This is a fail implementation
//    def index() {
//        List<Credit> credits = Credit.findAll()
//        credits.each {
//            def averageAmount = it.loanAmount / it.numberOfMonth
//            if (it.creditProgram.id == 1) {
//                it.finalResult = averageAmount
//            } else {
//                it.finalResult = (it.interestRate / 12) *  averageAmount
//            }
//        }
//        respond credits
//    }

    // is not used
    def calculate() {
        respond model: [
                credits: Credit.findAll(),
                creditPrograms: CreditProgram.findAll()
        ]
    }
}
