package ibs_lab1


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DepositController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Deposit.list(params), model: [depositInstanceCount: Deposit.count()]
    }

    def show(Deposit depositInstance) {
        respond depositInstance
    }

    def create() {
        respond new Deposit(params)
    }

    @Transactional
    def save(Deposit depositInstance) {
        if (depositInstance == null) {
            notFound()
            return
        }

        if (depositInstance.hasErrors()) {
            respond depositInstance.errors, view: 'create'
            return
        }

        depositInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'deposit.label', default: 'Deposit'), depositInstance.id])
                redirect depositInstance
            }
            '*' { respond depositInstance, [status: CREATED] }
        }
    }

    def edit(Deposit depositInstance) {
        respond depositInstance
    }

    @Transactional
    def update(Deposit depositInstance) {
        if (depositInstance == null) {
            notFound()
            return
        }

        if (depositInstance.hasErrors()) {
            respond depositInstance.errors, view: 'edit'
            return
        }

        depositInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Deposit.label', default: 'Deposit'), depositInstance.id])
                redirect depositInstance
            }
            '*' { respond depositInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Deposit depositInstance) {

        if (depositInstance == null) {
            notFound()
            return
        }

        depositInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Deposit.label', default: 'Deposit'), depositInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'deposit.label', default: 'Deposit'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
