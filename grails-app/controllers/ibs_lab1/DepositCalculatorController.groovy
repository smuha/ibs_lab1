package ibs_lab1

class DepositCalculatorController {

    def index() {
        List<Deposit> deposits = Deposit.findAll()
        // I'm very sorry about this piece of code. Yes, I know that it's sucks but I really haven't time
        // to sort out how to do it in appropriate way. I'm sorry..
        deposits.each {
            it.result = it.depositAmount + (it.depositAmount * it.interestRate * (it.frequency*30) / 365 * 100)
            if (it.depositType.alias == 'Deposit #1') {
                it.result *=  0.8
            } else if (it.depositType.alias == 'Deposit #2') {
                it.result *=  0.9
            } else if (it.depositType.alias == 'Deposit #3') {
                it.result *= 0.6
            }
        }
        respond deposits
    }
}
