package ibs_lab1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CounterpartyTypeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond CounterpartyType.list(params), model:[counterpartyTypeInstanceCount: CounterpartyType.count()]
    }

    def show(CounterpartyType counterpartyTypeInstance) {
        respond counterpartyTypeInstance
    }

    def create() {
        respond new CounterpartyType(params)
    }

    @Transactional
    def save(CounterpartyType counterpartyTypeInstance) {
        if (counterpartyTypeInstance == null) {
            notFound()
            return
        }

        if (counterpartyTypeInstance.hasErrors()) {
            respond counterpartyTypeInstance.errors, view:'create'
            return
        }

        counterpartyTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'counterpartyType.label', default: 'CounterpartyType'), counterpartyTypeInstance.id])
                redirect counterpartyTypeInstance
            }
            '*' { respond counterpartyTypeInstance, [status: CREATED] }
        }
    }

    def edit(CounterpartyType counterpartyTypeInstance) {
        respond counterpartyTypeInstance
    }

    @Transactional
    def update(CounterpartyType counterpartyTypeInstance) {
        if (counterpartyTypeInstance == null) {
            notFound()
            return
        }

        if (counterpartyTypeInstance.hasErrors()) {
            respond counterpartyTypeInstance.errors, view:'edit'
            return
        }

        counterpartyTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'CounterpartyType.label', default: 'CounterpartyType'), counterpartyTypeInstance.id])
                redirect counterpartyTypeInstance
            }
            '*'{ respond counterpartyTypeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(CounterpartyType counterpartyTypeInstance) {

        if (counterpartyTypeInstance == null) {
            notFound()
            return
        }

        counterpartyTypeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CounterpartyType.label', default: 'CounterpartyType'), counterpartyTypeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'counterpartyType.label', default: 'CounterpartyType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
