package ibs_lab1


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DepositInterestRateTypeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DepositInterestRateType.list(params), model: [depositInterestRateTypeInstanceCount: DepositInterestRateType.count()]
    }

    def show(DepositInterestRateType depositInterestRateTypeInstance) {
        respond depositInterestRateTypeInstance
    }

    def create() {
        respond new DepositInterestRateType(params)
    }

    @Transactional
    def save(DepositInterestRateType depositInterestRateTypeInstance) {
        if (depositInterestRateTypeInstance == null) {
            notFound()
            return
        }

        if (depositInterestRateTypeInstance.hasErrors()) {
            respond depositInterestRateTypeInstance.errors, view: 'create'
            return
        }

        depositInterestRateTypeInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'depositInterestRateType.label', default: 'DepositInterestRateType'), depositInterestRateTypeInstance.id])
                redirect depositInterestRateTypeInstance
            }
            '*' { respond depositInterestRateTypeInstance, [status: CREATED] }
        }
    }

    def edit(DepositInterestRateType depositInterestRateTypeInstance) {
        respond depositInterestRateTypeInstance
    }

    @Transactional
    def update(DepositInterestRateType depositInterestRateTypeInstance) {
        if (depositInterestRateTypeInstance == null) {
            notFound()
            return
        }

        if (depositInterestRateTypeInstance.hasErrors()) {
            respond depositInterestRateTypeInstance.errors, view: 'edit'
            return
        }

        depositInterestRateTypeInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DepositInterestRateType.label', default: 'DepositInterestRateType'), depositInterestRateTypeInstance.id])
                redirect depositInterestRateTypeInstance
            }
            '*' { respond depositInterestRateTypeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DepositInterestRateType depositInterestRateTypeInstance) {

        if (depositInterestRateTypeInstance == null) {
            notFound()
            return
        }

        depositInterestRateTypeInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DepositInterestRateType.label', default: 'DepositInterestRateType'), depositInterestRateTypeInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'depositInterestRateType.label', default: 'DepositInterestRateType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
