package ibs_lab1

import beans.CreditBean
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class CreditController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Credit.list(params), model: [creditInstanceCount: Credit.count()]
    }

    def show(Credit creditInstance) {
        respond creditInstance
    }

    def create() {
        respond new Credit(params)
    }

    def calculate(Credit credit) {
        def loanAmount = credit.loanAmount
        List<CreditBean> beans = new ArrayList<>()
        def totalPaidRaid = 0
        if (credit.creditProgram.programAlias == 'Program #1') {
            def payAmount = credit.payAmount
            def numberOfMonth = loanAmount / payAmount
            numberOfMonth.times {
                loanAmount -= payAmount
                payAmount = payAmount > loanAmount ? loanAmount : payAmount
                def paidRaid = loanAmount * credit.interestRate / 100
                totalPaidRaid += paidRaid
                beans.add(new CreditBean(
                        left: loanAmount,
                        paid: payAmount,
                        paidRaid: paidRaid
                ))
            }
        } else if (credit.creditProgram.programAlias == 'Program #2') {
            def payAmount = loanAmount / credit.numberOfMonth
            def number = credit.numberOfMonth - 1
            number.times {
                loanAmount -= payAmount
                payAmount = payAmount > loanAmount ? loanAmount : payAmount
                def paidRaid = loanAmount * credit.interestRate / 100
                totalPaidRaid += paidRaid
                beans.add(new CreditBean(
                        left: loanAmount,
                        paid: payAmount,
                        paidRaid: paidRaid
                ))
            }
        }

        render(view: "calculate", model: [
                beans: beans,
                totalPaidRaid: Math.round(totalPaidRaid * 100) / 100
        ])
    }

    @Transactional
    def save(Credit creditInstance) {
        if (creditInstance == null) {
            notFound()
            return
        }

        if (creditInstance.hasErrors()) {
            respond creditInstance.errors, view: 'create'
            return
        }

        creditInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'credit.label', default: 'Credit'), creditInstance.id])
                redirect creditInstance
            }
            '*' { respond creditInstance, [status: CREATED] }
        }
    }

    def edit(Credit creditInstance) {
        respond creditInstance
    }

    @Transactional
    def update(Credit creditInstance) {
        if (creditInstance == null) {
            notFound()
            return
        }

        if (creditInstance.hasErrors()) {
            respond creditInstance.errors, view: 'edit'
            return
        }

        creditInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Credit.label', default: 'Credit'), creditInstance.id])
                redirect creditInstance
            }
            '*' { respond creditInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Credit creditInstance) {

        if (creditInstance == null) {
            notFound()
            return
        }

        creditInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Credit.label', default: 'Credit'), creditInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'credit.label', default: 'Credit'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
