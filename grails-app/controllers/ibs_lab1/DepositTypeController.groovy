package ibs_lab1


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DepositTypeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DepositType.list(params), model: [depositTypeInstanceCount: DepositType.count()]
    }

    def show(DepositType depositTypeInstance) {
        respond depositTypeInstance
    }

    def create() {
        respond new DepositType(params)
    }

    @Transactional
    def save(DepositType depositTypeInstance) {
        if (depositTypeInstance == null) {
            notFound()
            return
        }

        if (depositTypeInstance.hasErrors()) {
            respond depositTypeInstance.errors, view: 'create'
            return
        }

        depositTypeInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'depositType.label', default: 'DepositType'), depositTypeInstance.id])
                redirect depositTypeInstance
            }
            '*' { respond depositTypeInstance, [status: CREATED] }
        }
    }

    def edit(DepositType depositTypeInstance) {
        respond depositTypeInstance
    }

    @Transactional
    def update(DepositType depositTypeInstance) {
        if (depositTypeInstance == null) {
            notFound()
            return
        }

        if (depositTypeInstance.hasErrors()) {
            respond depositTypeInstance.errors, view: 'edit'
            return
        }

        depositTypeInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DepositType.label', default: 'DepositType'), depositTypeInstance.id])
                redirect depositTypeInstance
            }
            '*' { respond depositTypeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DepositType depositTypeInstance) {

        if (depositTypeInstance == null) {
            notFound()
            return
        }

        depositTypeInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DepositType.label', default: 'DepositType'), depositTypeInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'depositType.label', default: 'DepositType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
