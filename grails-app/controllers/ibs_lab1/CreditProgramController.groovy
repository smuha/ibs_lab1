package ibs_lab1


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CreditProgramController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond CreditProgram.list(params), model: [creditProgramInstanceCount: CreditProgram.count()]
    }

    def show(CreditProgram creditProgramInstance) {
        respond creditProgramInstance
    }

    def create() {
        respond new CreditProgram(params)
    }

    @Transactional
    def save(CreditProgram creditProgramInstance) {
        if (creditProgramInstance == null) {
            notFound()
            return
        }

        if (creditProgramInstance.hasErrors()) {
            respond creditProgramInstance.errors, view: 'create'
            return
        }

        creditProgramInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'creditProgram.label', default: 'CreditProgram'), creditProgramInstance.id])
                redirect creditProgramInstance
            }
            '*' { respond creditProgramInstance, [status: CREATED] }
        }
    }

    def edit(CreditProgram creditProgramInstance) {
        respond creditProgramInstance
    }

    @Transactional
    def update(CreditProgram creditProgramInstance) {
        if (creditProgramInstance == null) {
            notFound()
            return
        }

        if (creditProgramInstance.hasErrors()) {
            respond creditProgramInstance.errors, view: 'edit'
            return
        }

        creditProgramInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'CreditProgram.label', default: 'CreditProgram'), creditProgramInstance.id])
                redirect creditProgramInstance
            }
            '*' { respond creditProgramInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(CreditProgram creditProgramInstance) {

        if (creditProgramInstance == null) {
            notFound()
            return
        }

        creditProgramInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CreditProgram.label', default: 'CreditProgram'), creditProgramInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'creditProgram.label', default: 'CreditProgram'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
