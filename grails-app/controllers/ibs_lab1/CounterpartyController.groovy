package ibs_lab1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CounterpartyController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Counterparty.list(params), model:[counterpartyInstanceCount: Counterparty.count()]
    }

    def show(Counterparty counterpartyInstance) {
        respond counterpartyInstance
    }

    def create() {
        respond new Counterparty(params)
    }

    @Transactional
    def save(Counterparty counterpartyInstance) {
        if (counterpartyInstance == null) {
            notFound()
            return
        }

        if (counterpartyInstance.hasErrors()) {
            respond counterpartyInstance.errors, view:'create'
            return
        }

        counterpartyInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'counterparty.label', default: 'Counterparty'), counterpartyInstance.id])
                redirect counterpartyInstance
            }
            '*' { respond counterpartyInstance, [status: CREATED] }
        }
    }

    def edit(Counterparty counterpartyInstance) {
        respond counterpartyInstance
    }

    @Transactional
    def update(Counterparty counterpartyInstance) {
        if (counterpartyInstance == null) {
            notFound()
            return
        }

        if (counterpartyInstance.hasErrors()) {
            respond counterpartyInstance.errors, view:'edit'
            return
        }

        counterpartyInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Counterparty.label', default: 'Counterparty'), counterpartyInstance.id])
                redirect counterpartyInstance
            }
            '*'{ respond counterpartyInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Counterparty counterpartyInstance) {

        if (counterpartyInstance == null) {
            notFound()
            return
        }

        counterpartyInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Counterparty.label', default: 'Counterparty'), counterpartyInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'counterparty.label', default: 'Counterparty'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
