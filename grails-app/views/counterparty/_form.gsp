<%@ page import="ibs_lab1.Counterparty" %>


<div class="fieldcontain ${hasErrors(bean: counterpartyInstance, field: 'userName', 'error')} required">
	<label for="userName">
		<g:message code="counterparty.userName.label" default="User Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="userName" required="" value="${counterpartyInstance?.userName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: counterpartyInstance, field: 'userSurName', 'error')} required">
	<label for="userSurName">
		<g:message code="counterparty.userSurName.label" default="User Sur Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="userSurName" required="" value="${counterpartyInstance?.userSurName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: counterpartyInstance, field: 'userMiddleName', 'error')} required">
	<label for="userMiddleName">
		<g:message code="counterparty.userMiddleName.label" default="User Middle Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="userMiddleName" required="" value="${counterpartyInstance?.userMiddleName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: counterpartyInstance, field: 'userAddress', 'error')} required">
	<label for="userAddress">
		<g:message code="counterparty.userAddress.label" default="User Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="userAddress" required="" value="${counterpartyInstance?.userAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: counterpartyInstance, field: 'workPlace', 'error')} required">
	<label for="workPlace">
		<g:message code="counterparty.workPlace.label" default="Work Place" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="workPlace" required="" value="${counterpartyInstance?.workPlace}" style="min-width: 500px"/>
</div>

<div class="fieldcontain ${hasErrors(bean: counterpartyInstance, field: 'counterpartyType', 'error')} required">
	<label for="counterpartyType">
		<g:message code="counterparty.counterpartyType.label" default="Counterparty Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="counterpartyType" name="counterpartyType.id" from="${ibs_lab1.CounterpartyType.list()}"
			  optionKey="id" required="" value="${counterpartyInstance?.counterpartyType?.id}" class="many-to-one"
			  optionValue="typeName"/>
</div>

