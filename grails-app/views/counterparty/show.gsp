
<%@ page import="ibs_lab1.Counterparty" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'counterparty.label', default: 'Counterparty')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-counterparty" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-counterparty" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list counterparty">
			
				<g:if test="${counterpartyInstance?.counterpartyType}">
				<li class="fieldcontain">
					<span id="counterpartyType-label" class="property-label"><g:message code="counterparty.counterpartyType.label" default="Counterparty Type" /></span>
					
						<span class="property-value" aria-labelledby="counterpartyType-label">
							<g:link controller="counterpartyType" action="show" id="${counterpartyInstance?.counterpartyType?.id}">
								${counterpartyInstance?.counterpartyType?.typeName}
							</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${counterpartyInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="counterparty.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${counterpartyInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${counterpartyInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="counterparty.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${counterpartyInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${counterpartyInstance?.userAddress}">
				<li class="fieldcontain">
					<span id="userAddress-label" class="property-label"><g:message code="counterparty.userAddress.label" default="User Address" /></span>
					
						<span class="property-value" aria-labelledby="userAddress-label"><g:fieldValue bean="${counterpartyInstance}" field="userAddress"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${counterpartyInstance?.userMiddleName}">
				<li class="fieldcontain">
					<span id="userMiddleName-label" class="property-label"><g:message code="counterparty.userMiddleName.label" default="User Middle Name" /></span>
					
						<span class="property-value" aria-labelledby="userMiddleName-label"><g:fieldValue bean="${counterpartyInstance}" field="userMiddleName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${counterpartyInstance?.userName}">
				<li class="fieldcontain">
					<span id="userName-label" class="property-label"><g:message code="counterparty.userName.label" default="User Name" /></span>
					
						<span class="property-value" aria-labelledby="userName-label"><g:fieldValue bean="${counterpartyInstance}" field="userName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${counterpartyInstance?.userSurName}">
				<li class="fieldcontain">
					<span id="userSurName-label" class="property-label"><g:message code="counterparty.userSurName.label" default="User Sur Name" /></span>
					
						<span class="property-value" aria-labelledby="userSurName-label"><g:fieldValue bean="${counterpartyInstance}" field="userSurName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${counterpartyInstance?.workPlace}">
				<li class="fieldcontain">
					<span id="workPlace-label" class="property-label"><g:message code="counterparty.workPlace.label" default="Work Place" /></span>
					
						<span class="property-value" aria-labelledby="workPlace-label"><g:fieldValue bean="${counterpartyInstance}" field="workPlace"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:counterpartyInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${counterpartyInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
