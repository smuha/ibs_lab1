
<%@ page import="ibs_lab1.Counterparty" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'counterparty.label', default: 'Counterparty')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-counterparty" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-counterparty" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
						<g:sortableColumn property="userName" title="${message(code: 'counterparty.userName.label', default: 'User Name')}" />
						<g:sortableColumn property="userName" title="${message(code: 'counterparty.userName.label', default: 'User Sur Name')}" />
						<g:sortableColumn property="userMiddleName" title="${message(code: 'counterparty.userMiddleName.label', default: 'User Middle Name')}" />
						<th><g:message code="counterparty.counterpartyType.label" default="Counterparty Type" /></th>
						<g:sortableColumn property="dateCreated" title="${message(code: 'counterparty.dateCreated.label', default: 'Date Created')}" />
						<g:sortableColumn property="userAddress" title="${message(code: 'counterparty.userAddress.label', default: 'User Address')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${counterpartyInstanceList}" status="i" var="counterpartyInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" id="${counterpartyInstance.id}">
							${counterpartyInstance.userName}
						</g:link></td>
						<td>${fieldValue(bean: counterpartyInstance, field: "userSurName")}</td>
						<td>${fieldValue(bean: counterpartyInstance, field: "userMiddleName")}</td>
						<td>${fieldValue(bean: counterpartyInstance.counterpartyType, field: "typeName")}</td>
						<td><g:formatDate date="${counterpartyInstance.dateCreated}" /></td>
						<td>${fieldValue(bean: counterpartyInstance, field: "userAddress")}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${counterpartyInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
