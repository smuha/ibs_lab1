
<%@ page import="ibs_lab1.CreditProgram" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'creditProgram.label', default: 'CreditProgram')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-creditProgram" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-creditProgram" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="programAlias" title="${message(code: 'creditProgram.programAlias.label', default: 'Program Alias')}" />
					
						<g:sortableColumn property="programDescription" title="${message(code: 'creditProgram.programDescription.label', default: 'Program Description')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${creditProgramInstanceList}" status="i" var="creditProgramInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${creditProgramInstance.id}">${fieldValue(bean: creditProgramInstance, field: "programAlias")}</g:link></td>
					
						<td>${fieldValue(bean: creditProgramInstance, field: "programDescription")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${creditProgramInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
