
<%@ page import="ibs_lab1.CreditProgram" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'creditProgram.label', default: 'CreditProgram')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-creditProgram" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-creditProgram" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list creditProgram">
			
				<g:if test="${creditProgramInstance?.programAlias}">
				<li class="fieldcontain">
					<span id="programAlias-label" class="property-label"><g:message code="creditProgram.programAlias.label" default="Program Alias" /></span>
					
						<span class="property-value" aria-labelledby="programAlias-label"><g:fieldValue bean="${creditProgramInstance}" field="programAlias"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creditProgramInstance?.programDescription}">
				<li class="fieldcontain">
					<span id="programDescription-label" class="property-label"><g:message code="creditProgram.programDescription.label" default="Program Description" /></span>
					
						<span class="property-value" aria-labelledby="programDescription-label"><g:fieldValue bean="${creditProgramInstance}" field="programDescription"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:creditProgramInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${creditProgramInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
