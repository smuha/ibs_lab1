<%@ page import="ibs_lab1.CreditProgram" %>



<div class="fieldcontain ${hasErrors(bean: creditProgramInstance, field: 'programAlias', 'error')} required">
	<label for="programAlias">
		<g:message code="creditProgram.programAlias.label" default="Program Alias" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="programAlias" required="" value="${creditProgramInstance?.programAlias}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: creditProgramInstance, field: 'programDescription', 'error')} required">
	<label for="programDescription">
		<g:message code="creditProgram.programDescription.label" default="Program Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="programDescription" required="" value="${creditProgramInstance?.programDescription}"/>

</div>

