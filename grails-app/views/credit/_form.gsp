<%@ page import="ibs_lab1.Credit" %>



<div class="fieldcontain ${hasErrors(bean: creditInstance, field: 'counterparty', 'error')} required">
	<label for="counterparty">
		<g:message code="credit.counterparty.label" default="Counterparty" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="counterparty" name="counterparty.id" from="${ibs_lab1.Counterparty.list()}" optionKey="id" required=""
			  value="${creditInstance?.counterparty?.id}" class="many-to-one" optionValue="userName"/>

</div>

<div class="fieldcontain ${hasErrors(bean: creditInstance, field: 'creditProgram', 'error')} required">
	<label for="creditProgram">
		<g:message code="credit.creditProgram.label" default="Credit Program" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="creditProgram" name="creditProgram.id" from="${ibs_lab1.CreditProgram.list()}" optionKey="id" required=""
			  value="${creditInstance?.creditProgram?.id}" class="many-to-one" optionValue="programAlias"/>

</div>

<div class="fieldcontain ${hasErrors(bean: creditInstance, field: 'interestRate', 'error')} required">
	<label for="interestRate">
		<g:message code="credit.interestRate.label" default="Interest Rate" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="interestRate" value="${fieldValue(bean: creditInstance, field: 'interestRate')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: creditInstance, field: 'loanAmount', 'error')} required">
	<label for="loanAmount">
		<g:message code="credit.loanAmount.label" default="Loan Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="loanAmount" type="number" value="${creditInstance.loanAmount}" required=""/>

</div>

<g:if test="${creditInstance?.creditProgram.programAlias == 'Program #1'}">
	<div class="fieldcontain ${hasErrors(bean: creditInstance, field: 'payAmount', 'error')} required">
	<label for="payAmount">
		<g:message code="credit.numberOfMonth.label" default="Pay Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="payAmount" type="number" value="${creditInstance.payAmount}" required=""/>
</g:if>

</div>

