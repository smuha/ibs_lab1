
<%@ page import="ibs_lab1.Credit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'credit.label', default: 'Credit')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-credit" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-credit" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
						<th><g:message code="credit.counterparty.label" default="Counterparty" /></th>
						<th><g:message code="credit.creditProgram.label" default="Credit Program" /></th>
						<g:sortableColumn property="dateCreated" title="${message(code: 'credit.dateCreated.label', default: 'Date Created')}" />
						<g:sortableColumn property="interestRate" title="${message(code: 'credit.interestRate.label', default: 'Interest Rate')}" />
						<g:sortableColumn property="lastUpdated" title="${message(code: 'credit.lastUpdated.label', default: 'Number of month')}" />
						<g:sortableColumn property="loanAmount" title="${message(code: 'credit.loanAmount.label', default: 'Loan Amount')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${creditInstanceList}" status="i" var="creditInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" id="${creditInstance.id}">
							${fieldValue(bean: creditInstance.counterparty, field: "userName")}
						</g:link></td>
						<td>${fieldValue(bean: creditInstance.creditProgram, field: "programAlias")}</td>
						<td><g:formatDate date="${creditInstance.dateCreated}" /></td>
						<td>${fieldValue(bean: creditInstance, field: "interestRate")}</td>
						<td>${fieldValue(bean: creditInstance, field: "numberOfMonth")}</td>
						<td>${fieldValue(bean: creditInstance, field: "loanAmount")}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${creditInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
