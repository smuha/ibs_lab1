
<%@ page import="ibs_lab1.Credit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'credit.label', default: 'Credit')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-credit" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-credit" class="content scaffold-list" role="main">
			<h1>Credit Calculator</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
						<th style="width: 50px;"><g:message code="credit.counterparty.label" default="Місяць" /></th>
						<th><g:message code="credit.counterparty.label" default="Залишок" /></th>
						<th><g:message code="credit.creditProgram.label" default="Оплата" /></th>
						<th><g:message code="credit.creditProgram.label" default="Оплата відсотків" /></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${beans}" status="i" var="bean">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${i}</td>
						<td>${bean.left}</td>
						<td>${bean.paid}</td>
						<td>${bean.paidRaid}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${creditInstanceCount ?: 0}" />
			</div>
			<h3 style="padding: 15px 0 0 30px">Сума оплачених відсотків = ${totalPaidRaid}</h3>
		</div>
	</body>
</html>
