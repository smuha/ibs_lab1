
<%@ page import="ibs_lab1.Credit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'credit.label', default: 'Credit')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-credit" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-credit" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list credit">
			
				<g:if test="${creditInstance?.counterparty}">
				<li class="fieldcontain">
					<span id="counterparty-label" class="property-label"><g:message code="credit.counterparty.label" default="Counterparty" /></span>
						<span class="property-value" aria-labelledby="counterparty-label">
							<g:link controller="counterparty" action="show" id="${creditInstance?.counterparty?.id}">
								${creditInstance?.counterparty?.userName}
							</g:link>
						</span>
				</li>
				</g:if>
			
				<g:if test="${creditInstance?.creditProgram}">
				<li class="fieldcontain">
					<span id="creditProgram-label" class="property-label"><g:message code="credit.creditProgram.label" default="Credit Program" /></span>
						<span class="property-value" aria-labelledby="creditProgram-label">
							<g:link controller="creditProgram" action="show" id="${creditInstance?.creditProgram?.id}">
								${creditInstance?.creditProgram?.programAlias}
							</g:link>
						</span>
					
				</li>
				</g:if>
			
				<g:if test="${creditInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="credit.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${creditInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${creditInstance?.interestRate}">
				<li class="fieldcontain">
					<span id="interestRate-label" class="property-label"><g:message code="credit.interestRate.label" default="Interest Rate" /></span>
					
						<span class="property-value" aria-labelledby="interestRate-label"><g:fieldValue bean="${creditInstance}" field="interestRate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creditInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="credit.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${creditInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${creditInstance?.loanAmount}">
				<li class="fieldcontain">
					<span id="loanAmount-label" class="property-label"><g:message code="credit.loanAmount.label" default="Loan Amount" /></span>
					
						<span class="property-value" aria-labelledby="loanAmount-label"><g:fieldValue bean="${creditInstance}" field="loanAmount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creditInstance?.numberOfMonth}">
				<li class="fieldcontain">
					<span id="numberOfMonth-label" class="property-label"><g:message code="credit.numberOfMonth.label" default="Number Of Month" /></span>
						<span class="property-value" aria-labelledby="numberOfMonth-label"><g:fieldValue bean="${creditInstance}" field="numberOfMonth"/></span>
				</li>
				</g:if>
				<g:if test="${creditInstance?.creditProgram.programAlias == 'Program #1'}">
					<li class="fieldcontain">
						<span id="payAmount-label" class="property-label"><g:message code="credit.numberOfMonth.label" default="Pay Amount" /></span>
						<span class="property-value" aria-labelledby="payAmount-label"><g:fieldValue bean="${creditInstance}" field="payAmount"/></span>
					</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:creditInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${creditInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					<g:link  action="calculate" resource="${creditInstance}"><g:message code="default.button.calculate.label" default="Calculate" /></g:link>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
