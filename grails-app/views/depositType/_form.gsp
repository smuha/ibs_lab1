<%@ page import="ibs_lab1.DepositType" %>



<div class="fieldcontain ${hasErrors(bean: depositTypeInstance, field: 'alias', 'error')} required">
    <label for="alias">
        <g:message code="depositType.alias.label" default="Alias"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="alias" required="" value="${depositTypeInstance?.alias}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositTypeInstance, field: 'description', 'error')} required">
    <label for="description">
        <g:message code="depositType.description.label" default="Description"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="description" required="" value="${depositTypeInstance?.description}"/>

</div>

