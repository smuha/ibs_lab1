<%@ page import="ibs_lab1.Credit" %>
<%@ page import="ibs_lab1.CreditProgram" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'credit.label', default: 'Credit')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-credit" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
	<div id="creditCalculator" class="content scaffold-create" role="main">
		<h1><g:message code="default.create.label" args="[entityName]" /></h1>
		<g:form url="[resource:[crdits, creditPrograms], action:'calculate']" >
			<fieldset class="form">
				<label for="creditProgram">
					<g:message code="credit.creditProgram.label" default="Credit Program" />
					<span class="required-indicator">*</span>
				</label>
				<g:select id="creditProgram" name="creditProgram" from="${ibs_lab1.CreditProgram.list()}" optionKey="id"
						   class="many-to-one" optionValue="programAlias"/>
				<br/><br/>
				<label for="credit">
					<g:message code="credit.creditProgram.label" default="Credit" />
					<span class="required-indicator">*</span>
				</label>
				<g:select id="credit" name="credit" from="${ibs_lab1.Credit.list()}" optionKey="id"
						  class="many-to-one" optionValue="${{it.counterparty?.userName + ' ' + it.counterparty?.userSurName +
						' (' + it.creditProgram?.programAlias + ')'}}"/>
			</fieldset>
			<fieldset class="buttons">
				<g:submitButton name="create" class="save" value="${message(code: 'default.button.calculate.label', default: 'Calculate')}" />
			</fieldset>
		</g:form>
	</div>
	</body>
</html>
