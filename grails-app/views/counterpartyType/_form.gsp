<%@ page import="ibs_lab1.CounterpartyType" %>



<div class="fieldcontain ${hasErrors(bean: counterpartyTypeInstance, field: 'typeName', 'error')} required">
	<label for="typeName">
		<g:message code="counterpartyType.typeName.label" default="Type Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="typeName" required="" value="${counterpartyTypeInstance?.typeName}"/>

</div>

