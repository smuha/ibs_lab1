<%@ page import="ibs_lab1.Deposit" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'deposit.label', default: 'Deposit')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-deposit" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-deposit" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list deposit">

        <g:if test="${depositInstance?.counterparty}">
            <li class="fieldcontain">
                <span id="counterparty-label" class="property-label"><g:message code="deposit.counterparty.label"
                                                                                default="Counterparty"/></span>

                <span class="property-value" aria-labelledby="counterparty-label"><g:link controller="counterparty"
                                                                                          action="show"
                                                                                          id="${depositInstance?.counterparty?.id}">${depositInstance?.counterparty?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${depositInstance?.depositAmount}">
            <li class="fieldcontain">
                <span id="depositAmount-label" class="property-label"><g:message code="deposit.depositAmount.label"
                                                                                 default="Deposit Amount"/></span>

                <span class="property-value" aria-labelledby="depositAmount-label"><g:fieldValue
                        bean="${depositInstance}" field="depositAmount"/></span>

            </li>
        </g:if>

        <g:if test="${depositInstance?.depositTerm}">
            <li class="fieldcontain">
                <span id="depositTerm-label" class="property-label"><g:message code="deposit.depositTerm.label"
                                                                               default="Deposit Term"/></span>

                <span class="property-value" aria-labelledby="depositTerm-label"><g:fieldValue bean="${depositInstance}"
                                                                                               field="depositTerm"/></span>

            </li>
        </g:if>

        <g:if test="${depositInstance?.depositType}">
            <li class="fieldcontain">
                <span id="depositType-label" class="property-label"><g:message code="deposit.depositType.label"
                                                                               default="Deposit Type"/></span>

                <span class="property-value" aria-labelledby="depositType-label"><g:link controller="depositType"
                                                                                         action="show"
                                                                                         id="${depositInstance?.depositType?.id}">${depositInstance?.depositType?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${depositInstance?.frequency}">
            <li class="fieldcontain">
                <span id="frequency-label" class="property-label"><g:message code="deposit.frequency.label"
                                                                             default="Frequency"/></span>

                <span class="property-value" aria-labelledby="frequency-label"><g:fieldValue bean="${depositInstance}"
                                                                                             field="frequency"/></span>

            </li>
        </g:if>

        <g:if test="${depositInstance?.interestRate}">
            <li class="fieldcontain">
                <span id="interestRate-label" class="property-label"><g:message code="deposit.interestRate.label"
                                                                                default="Interest Rate"/></span>

                <span class="property-value" aria-labelledby="interestRate-label"><g:fieldValue
                        bean="${depositInstance}" field="interestRate"/></span>

            </li>
        </g:if>

        <g:if test="${depositInstance?.interestType}">
            <li class="fieldcontain">
                <span id="interestType-label" class="property-label"><g:message code="deposit.interestType.label"
                                                                                default="Interest Type"/></span>

                <span class="property-value" aria-labelledby="interestType-label"><g:link controller="interestType"
                                                                                          action="show"
                                                                                          id="${depositInstance?.interestType?.id}">${depositInstance?.interestType?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: depositInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${depositInstance}"><g:message code="default.button.edit.label"
                                                                                        default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
