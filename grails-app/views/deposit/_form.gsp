<%@ page import="ibs_lab1.DepositInterestRateType; ibs_lab1.InterestRateType; ibs_lab1.Deposit" %>



<div class="fieldcontain ${hasErrors(bean: depositInstance, field: 'counterparty', 'error')} required">
    <label for="counterparty">
        <g:message code="deposit.counterparty.label" default="Counterparty"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="counterparty" name="counterparty.id" from="${ibs_lab1.Counterparty.list()}" optionKey="id" required=""
              value="${depositInstance?.counterparty?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositInstance, field: 'depositAmount', 'error')} required">
    <label for="depositAmount">
        <g:message code="deposit.depositAmount.label" default="Deposit Amount"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="depositAmount" value="${fieldValue(bean: depositInstance, field: 'depositAmount')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositInstance, field: 'depositTerm', 'error')} required">
    <label for="depositTerm">
        <g:message code="deposit.depositTerm.label" default="Deposit Term"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="depositTerm" type="number" value="${depositInstance.depositTerm}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositInstance, field: 'depositType', 'error')} required">
    <label for="depositType">
        <g:message code="deposit.depositType.label" default="Deposit Type"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="depositType" name="depositType.id" from="${ibs_lab1.DepositType.list()}" optionKey="id" required=""
              value="${depositInstance?.depositType?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositInstance, field: 'frequency', 'error')} required">
    <label for="frequency">
        <g:message code="deposit.frequency.label" default="Frequency"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="frequency" type="number" value="${depositInstance.frequency}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositInstance, field: 'interestRate', 'error')} required">
    <label for="interestRate">
        <g:message code="deposit.interestRate.label" default="Interest Rate"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="interestRate" value="${fieldValue(bean: depositInstance, field: 'interestRate')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositInstance, field: 'interestRateType', 'error')} required">
    <label for="interestType">
        <g:message code="deposit.interestType.label" default="Interest Type"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="interestType" name="interestType.id" from="${ibs_lab1.DepositInterestRateType.list()}" optionKey="id" required=""
              value="${depositInstance?.interestType?.id}" class="many-to-one"/>

</div>

