<%@ page import="ibs_lab1.Deposit" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'deposit.label', default: 'Deposit')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-deposit" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-deposit" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <th><g:message code="deposit.counterparty.label" default="Counterparty"/></th>

            <g:sortableColumn property="depositAmount"
                              title="${message(code: 'deposit.depositAmount.label', default: 'Deposit Amount')}"/>

            <g:sortableColumn property="depositTerm"
                              title="${message(code: 'deposit.depositTerm.label', default: 'Deposit Term')}"/>

            <th><g:message code="deposit.depositType.label" default="Deposit Type"/></th>

            <g:sortableColumn property="frequency"
                              title="${message(code: 'deposit.frequency.label', default: 'Frequency')}"/>

            <g:sortableColumn property="interestRate"
                              title="${message(code: 'deposit.interestRate.label', default: 'Interest Rate')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${depositInstanceList}" status="i" var="depositInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show" id="${depositInstance.id}">
                    ${fieldValue(bean: depositInstance.counterparty, field: "userName")}
                </g:link></td>
                <td>${fieldValue(bean: depositInstance, field: "depositAmount")}</td>
                <td>${fieldValue(bean: depositInstance, field: "depositTerm")}</td>
                <td>${fieldValue(bean: depositInstance.depositType, field: "alias")}</td>
                <td>${fieldValue(bean: depositInstance, field: "interestRate")}</td>
                <td>${fieldValue(bean: depositInstance, field: "frequency")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${depositInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
