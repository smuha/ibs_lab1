
<%@ page import="ibs_lab1.DepositInterestRateType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'depositInterestRateType.label', default: 'DepositInterestRateType')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-depositInterestRateType" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-depositInterestRateType" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="alias" title="${message(code: 'depositInterestRateType.alias.label', default: 'Alias')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'depositInterestRateType.description.label', default: 'Description')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${depositInterestRateTypeInstanceList}" status="i" var="depositInterestRateTypeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${depositInterestRateTypeInstance.id}">${fieldValue(bean: depositInterestRateTypeInstance, field: "alias")}</g:link></td>
					
						<td>${fieldValue(bean: depositInterestRateTypeInstance, field: "description")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${depositInterestRateTypeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
