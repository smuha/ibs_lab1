<%@ page import="ibs_lab1.DepositInterestRateType" %>



<div class="fieldcontain ${hasErrors(bean: depositInterestRateTypeInstance, field: 'alias', 'error')} required">
	<label for="alias">
		<g:message code="depositInterestRateType.alias.label" default="Alias" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="alias" required="" value="${depositInterestRateTypeInstance?.alias}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: depositInterestRateTypeInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="depositInterestRateType.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${depositInterestRateTypeInstance?.description}"/>

</div>

